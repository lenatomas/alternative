## Run

To start the service locally on port 8080 run:

```
 npm install OR yarn
 npm run start OR yarn START

 - go to http://localhost:8080/

```

## Build

```
 npm run build OR yarn build
```
