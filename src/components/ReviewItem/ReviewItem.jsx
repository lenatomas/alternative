import React from "react";
import stars from "../../../images/5stars.png";
import "./review-item.less";

const ReviewItem = ({ caption, comment }) => (
    <div className="review-item">
        <div>
            <img src={stars} />
        </div>
        <div className="review-caption">{`"${caption}"`}</div>
        <div className="review-comment">{comment}</div>
    </div>
);

export default ReviewItem;
