import React from "react";
import "./faq-item.less";

const FaqItem = ({ title, content }) => (
    <div className="faq-item">
        <div className="faq-title">{title}</div>
        <div className="faq-content">{content}</div>
    </div>
);

export default FaqItem;
