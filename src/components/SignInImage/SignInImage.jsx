import React from "react";
import paypalWhite from "../../../images/paypal-white.png";
import "./signin-image.less";

const getComponentIndex = id => {
    switch (id) {
        case "plane":
            return 1;
        case "office":
            return 2;
        case "fitness":
            return 3;
    }
};

class SignInImages extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            hoveringValue: 0
        };
    }
    render () {
        const { imageSrc, id, hoveringValue, changeHover } = this.props;

        const componentIndex = getComponentIndex(id);

        return (
            <div
                id={id}
                className="paypal-signin"
                onMouseEnter={e => changeHover(e, componentIndex)}
                onMouseLeave={e => changeHover(e, 0)}
            >
                {componentIndex !== hoveringValue ? (
                    <img src={imageSrc} />
                ) : (
                    <div className="paypal-login__image">
                        <img src={paypalWhite} />
                        <span className="signin-button">Sign In</span>
                    </div>
                )}
            </div>
        );
    }
}

export default SignInImages;
