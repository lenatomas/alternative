import React from "react";
import "./flight-types.less";

class FlightTypes extends React.Component {
    render () {
        return (
            <div className="flight-options">
                <label className="radio-option">
          Return
                    <input type="radio" name="flight" value="return" defaultChecked />
                    <span className="checkmark" />
                </label>
                <label className="radio-option">
          One way
                    <input type="radio" name="flight" value="oneWay" />
                    <span className="checkmark" />
                </label>
                <label className="radio-option">
          Multi-city
                    <input type="radio" name="flight" value="multiCity" />
                    <span className="checkmark" />
                </label>
            </div>
        );
    }
}

export default FlightTypes;
