import React from "react";
import "./nav-item.less";

const NavItem = ({ title }) => <div className="nav-item">{title}</div>;

export default NavItem;
