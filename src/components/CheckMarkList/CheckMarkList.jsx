import React from "react";
import "./check-mark-list.less";

const CheckMarkList = ({ text, id }) => (
    <div className="check-mark__list">
        <span className="check" />
        <div className="list-item">{text}</div>
    </div>
);

export default CheckMarkList;
