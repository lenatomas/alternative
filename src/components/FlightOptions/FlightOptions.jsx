import React from "react";
import "./flight-options.less";

class FlightOptions extends React.Component {
    render () {
        return (
            <div className="flight-options">
                <span className="input-fields">
                    <span>From</span>
                    <input
                        type="text"
                        name="from"
                        defaultValue="Palma Mallorca (PMI)"
                        placeholder="Select airport..."
                    />
                </span>
                <span className="input-fields">
                    <span>To</span>
                    <input
                        type="text"
                        name="to"
                        defaultValue="London (All Airports) (LON)"
                        placeholder="Select airport..."
                    />
                </span>
                <span className="input-fields">
                    <span>Depart</span>
                    <input type="date" name="depart" />
                </span>
                <span className="input-fields">
                    <span>Return</span>
                    <input type="date" name="return" />
                </span>
                <span className="input-fields">
                    <span>{`Cabin Class & Travellers`}</span>
                    <input type="text" name="to" defaultValue="1 Adult, Economy" />
                </span>
            </div>
        );
    }
}

export default FlightOptions;
