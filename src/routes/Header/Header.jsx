import React from "react";
import NavItem from "../../components/NavItem/NavItem";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import logo from "../../../images/logo-blue-big.png";
import euroFlag from "../../../images/eur.png";
import "./header.less";

class Header extends React.Component {
    render () {
        return (
            <div className="nav-bar">
                <img className="logo" src={logo} />
                <div className="nav-group">
                    <NavItem title="Why use us" />
                    <NavItem title="Groups" />
                    <NavItem title="Contact" />
                </div>
                <div className="nav-group__user">
                    <NavItem title="Sign up" />
                    <NavItem title="Login" />
                    <div className="nav-item">
                        <img src={euroFlag} />
                        <span className="language">EUR</span>
                        <FontAwesomeIcon icon={faChevronDown} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;
