import React from "react";
import ReviewItem from "../../components/ReviewItem/ReviewItem";
import "./reviews.less";

const reviews = [
    {
        id: "firstCap",
        caption: "It's a breeze and could use my PayPal account",
        comment: [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        ]
    },
    {
        id: "secondCap",
        caption: "First time user, will come back!! Great Prices!",
        comment: [
            "Odio pellentesque diam volutpat commodo sed. Proin sed libero enim sed faucibus turpis in eu. "
        ]
    },
    {
        id: "thirdCap",
        caption: "Problem free",
        comment: [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio pellentesque diam volutpat commodo sed. Proin sed libero enim sed faucibus turpis in eu. "
        ]
    },
    {
        id: "fourthCap",
        caption: "Great that Alternative Airlines accepts Paypal",
        comment: ["Lorem ipsum dolor sit amet."]
    }
];

class Reviews extends React.Component {
    render () {
        return (
            <div className="reviews">
                <div className="reviews-content">
                    <div className="title">
                        <span className="baby-blue">We love that</span> you love paypal
                    </div>
                    {reviews.map(({ caption, comment, id }) => (
                        <ReviewItem key={id} caption={caption} comment={comment} />
                    ))}
                </div>
            </div>
        );
    }
}

export default Reviews;
