import React from "react";
import CheckMarkList from "../../components/CheckMarkList/CheckMarkList";
import "./paypal-advantages.less";

const textContent = [
    {
        id: "first",
        text: [
            "Flight Paypal payments done easily with the Alternative Airlines. An online flight search that offers domestic and international flights to anywhere you want to go."
        ]
    },
    {
        id: "second",
        text: [
            "Pay for flights and travel with PayPal with just an e-mail address and password. Don't carry those pastic bank cards around."
        ]
    },
    {
        id: "third",
        text: [
            "Choose to pay flights with PayPal now in one full transaction or later with PayPal Credit"
        ]
    },
    {
        id: "fourth",
        text: [
            "Pay flights with PayPal with logs of ",
            <span key="fourth-span" className="primary-bold">
        PayPal promo coupons
            </span>,
            " and voucher codes."
        ]
    },
    {
        id: "fifth",
        text: [
            "Use PayPal anywhere in the world with Alternative Airlines and find the flights you want."
        ]
    }
];

class PaypalAdvantages extends React.Component {
    render () {
        return (
            <div className="paypal-advantages">
                <div className="list-title">
                    <span>PayPal and PayPal Credit</span> Best ways to pay for flight
          tickets
                </div>
                {textContent.map(({ text, id }) => (
                    <CheckMarkList key={id} id={id} text={text} />
                ))}
            </div>
        );
    }
}

export default PaypalAdvantages;
