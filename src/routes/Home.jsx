import React from "react";
import moment from "moment";
import Header from "./Header/Header";
import FlightSearch from "./FlightSearch/FlightSearch";
import PaypalHero from "./PaypalHero/PaypalHero";
import PaypalAdvantages from "./PaypalAdvantages/PaypalAdvantages";
import PurchaseInformation from "./PurchaseInformation/PurchaseInformation";
import PayPalFaqs from "./PayPalFaqs/PayPalFaqs";
import Reviews from "./Reviews/Reviews";
import { withCookies } from "react-cookie";

class Home extends React.Component {
    constructor (props) {
        super(props);
    }
    componentDidMount () {
        const { cookies } = this.props;
        cookies.set("paypal", 1, {
            path: "/",
            expires: moment()
                .add(30, "days")
                .toDate()
        });
    }
    render () {
        return (
            <div className="main-page">
                <Header />
                <FlightSearch />
                <PaypalHero />
                <PaypalAdvantages />
                <PurchaseInformation />
                <PayPalFaqs />
                <Reviews />
            </div>
        );
    }
}
export default withCookies(Home);
