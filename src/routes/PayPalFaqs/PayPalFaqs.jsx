import React from "react";
import FaqItem from "../../components/FaqItem/FaqItem";
import "./paypal-faqs.less";

const faqContent = [
    {
        id: "one",
        title: "What is PayPal?",
        content: [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio pellentesque diam volutpat commodo sed. Proin sed libero enim sed faucibus turpis in eu. "
        ]
    },
    {
        id: "two",
        title: "Why use PayPal?",
        content: [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio pellentesque diam volutpat commodo sed. Proin sed libero enim sed faucibus turpis in eu. "
        ]
    },
    {
        id: "three",
        title: "Why should I use PayPal?",
        content: [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio pellentesque diam volutpat commodo sed. Proin sed libero enim sed faucibus turpis in eu. "
        ]
    }
];

class PayPalFaqs extends React.Component {
    render () {
        return (
            <div className="paypal-faqs">
                <div className="faqs-title">
          Paypal <span className="highlight">faqs</span>
                </div>
                {faqContent.map(({ title, content, id }) => (
                    <FaqItem key={id} title={title} content={content} />
                ))}
            </div>
        );
    }
}

export default PayPalFaqs;
