import React from "react";
import FlightTypes from "../../components/FlightTypes/FlightTypes";
import FlightOptions from "../../components/FlightOptions/FlightOptions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlane } from "@fortawesome/free-solid-svg-icons";
import "./flight-search.less";

class FlightSearch extends React.Component {
    render () {
        return (
            <div className="flight-search">
                <div className="content">
                    <div className="hero-title">
            Search and Buy Flights with PayPal and PayPal Credit
                    </div>
                    <div className="flight-search__widget">
                        <div className="flight-types">
                            <FlightTypes />
                        </div>
                        <div>
                            <FlightOptions />
                        </div>

                        <button className="search-flights">
                            <span>Search flights</span>
                            <FontAwesomeIcon icon={faPlane} />
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default FlightSearch;
