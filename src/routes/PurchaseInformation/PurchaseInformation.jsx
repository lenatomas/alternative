import React from "react";
import planeImage from "../../../images/plane-841441_1280.jpg";
import officeImage from "../../../images/office-620822_1280.jpg";
import fitnessImage from "../../../images/fitness-332278_1280.jpg";
import SignInImage from "../../components/SignInImage/SignInImage";
import paypalBanner from "../../../images/paypal-credit-banner-wide.gif";
import "./purchase-information.less";

const textContent = {
    first: [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quam elementum pulvinar etiam non quam lacus suspendisse."
    ],
    second: [
        "Nec feugiat nisl pretium fusce id velit. Gravida cum sociis natoque penatibus et magnis dis parturient. Vel quam elementum pulvinar etiam non quam lacus. Molestie at elementum eu facilisis sed. Neque viverra justo nec ultrices dui sapien eget mi proin. Duis convallis convallis tellus id. Tempus iaculis urna id volutpat. Non blandit massa enim nec dui. Amet aliquam id diam maecenas ultricies mi eget. Egestas congue quisque egestas diam in arcu. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor posuere."
    ],
    third: [
        "At erat pellentesque adipiscing ",
        <span key="first-span" className="primary-bold">
      commodo elit
        </span>,
        " at imperdiet dui accumsan. Adipiscing ",
        <span key="second-span" className="primary-bold">
      bibendum est ultricies
        </span>,
        " integer quis auctor elit sed. Volutpat ac tincidunt vitae semper. Tellus id interdum velit laoreet id donec ultrices."
    ]
};

class PurchaseInformation extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            hoveringValue: 0
        };
    }

    changeHover (e, index) {
        e.stopPropagation();
        e.preventDefault();
        this.setState({ hoveringValue: index });
    }

    render () {
        const { hoveringValue } = this.state;
        return (
            <div className="purchase-information">
                <div className="purchase-information__content">
                    <div className="title">
            Purchasing flights with Alternative Airlines just became even easier
            with PayPal
                    </div>
                    <div className="paragraph">{textContent.first}</div>
                    <div className="paragraph">{textContent.second}</div>
                    <div className="image-block">
                        <SignInImage
                            id="plane"
                            imageSrc={planeImage}
                            hoveringValue={hoveringValue}
                            changeHover={(e, index) => this.changeHover(e, index)}
                        />
                        <SignInImage
                            id="office"
                            imageSrc={officeImage}
                            hoveringValue={hoveringValue}
                            changeHover={(e, index) => this.changeHover(e, index)}
                        />
                        <SignInImage
                            id="fitness"
                            imageSrc={fitnessImage}
                            hoveringValue={hoveringValue}
                            changeHover={(e, index) => this.changeHover(e, index)}
                        />
                    </div>
                    <div className="paragraph">{textContent.third}</div>
                    <div className="paypal-banner">
                        <img src={paypalBanner} />
                    </div>
                </div>
            </div>
        );
    }
}

export default PurchaseInformation;
