import React from "react";
import paypalBlue from "../../../images/paypal-big.png";

import "./paypal-hero.less";

class PaypalHero extends React.Component {
    render () {
        return (
            <div className="paypal-hero">
                <div className="hero-content">
                    <img src={paypalBlue} />
                    <div className="slogan">
                        <div className="title">
              Paying for flights made easier with paypal
                        </div>
                        <div className="values">The safer, easier way to pay</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PaypalHero;
