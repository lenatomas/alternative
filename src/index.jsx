import React from "react";
import ReactDOM from "react-dom";

import { CookiesProvider } from "react-cookie";
import Home from "./routes/Home";

import "./index.less";

class Welcome extends React.Component {
    render () {
        return (
            <CookiesProvider>
                <Home />
            </CookiesProvider>
        );
    }
}
ReactDOM.render(<Welcome />, document.getElementById("root"));
